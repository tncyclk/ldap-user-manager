from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from ldap3 import AUTO_BIND_NO_TLS, SIMPLE

app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
    SQLALCHEMY_DATABASE_URI='sqlite://',
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    SECRET_KEY='very-secret-key',
    WTF_CSRF_SECRET_KEY='csrf-secret-key',
    LDAP_HOST='localhost',
    LDAP_PORT=389,
    LDAP_AUTO_BIND=AUTO_BIND_NO_TLS,
    LDAP_AUTHENTICATION=SIMPLE,
    LDAP_BASE_DN='ou=people,dc=localhost,dc=example',
    LDAP_USER_TEMPLATE='cn={0},ou=people,dc=localhost,dc=example',
    CONTACT_EMAIL='postmaster@localhost.example',
    APP_TITLE='Account Manager',
    PASSWORD_SCHEME='ldap_pbkdf2_sha512'))

app.config.from_envvar('FLASK_SETTINGS', silent=True)

db = SQLAlchemy(app)
from .auth.models import User

db.init_app(app)
db.create_all()

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'auth.login'

from ldap_user_manager.auth.views import auth
app.register_blueprint(auth)
