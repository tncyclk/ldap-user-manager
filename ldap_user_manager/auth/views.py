import logging

from flask import request, render_template, flash, redirect, \
    url_for, Blueprint, g
from flask_login import current_user, login_user, \
    logout_user, login_required
from ..ldap_user_manager import login_manager, db
from ldap_user_manager.auth.models import User, LoginForm, SettingsForm
from passlib.context import CryptContext
from ..ldap_user_manager import app

logging.basicConfig(level=logging.ERROR)

auth = Blueprint('auth', __name__)


def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash('{0}: {1}'.format(
                getattr(form, field).label.text, error), 'danger')


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@auth.before_request
def get_current_user():
    g.user = current_user


@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect(url_for('auth.login'))


@auth.route('/')
@auth.route('/settings')
@login_required
def settings():
    form = SettingsForm(request.form)
    return render_template('settings.html', form=form)


@auth.route('/save', methods=['POST'])
@login_required
def save():
    form = SettingsForm()

    if form.validate_on_submit():
        current_password = request.form.get('current_password')
        sieve = request.form.get('sieve') if request.form.get(
            'sieve') is not None else ''
        spamassassin = request.form.get('spamassassin') if request.form.get(
            'spamassassin') is not None else ''
        new_password = request.form.get('new_password')

        new_password_hash = None

        if new_password is not '':
            pwd_context = CryptContext(schemes=[app.config['PASSWORD_SCHEME']])
            new_password_hash = pwd_context.encrypt(new_password)

        try:
            g.user.save(current_password, sieve,
                        spamassassin, new_password_hash)
        except:
            flash('Invalid  password. Please try again.', 'danger')

        flash('Updated your settings', 'info')
    else:
        flash_errors(form)

    return redirect(url_for('auth.settings'))


@auth.route('/configuration')
@login_required
def configuration():
    return render_template('configuration.html')


@auth.route('/contact')
@login_required
def contact():
    return render_template('contact.html')


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('auth.settings'))

    form = LoginForm(request.form)

    if request.method == 'POST' and form.validate():
        username = request.form.get('username')
        password = request.form.get('password')

        try:
            attributes = User.try_login(username, password)

            user_groups = []
            for raw_group in attributes['memberOf']:
                for group_attribute in raw_group.split(','):
                    if group_attribute.startswith('cn'):
                        group_name = group_attribute.split('=')[1]
                        user_groups.append(group_name)

            sieve_rules = attributes['sieve'][
                0] if 'sieve' in attributes and attributes['sieve'] else ''
            spamassassin_rules = attributes[
                'spamassassin'] if 'spamassassin' in attributes else []

            user = User(
                username,
                password,
                sieve_rules,
                '\n'.join(spamassassin_rules),
                'mail' in user_groups)

            db.session.add(user)
            db.session.commit()
        except Exception as e:
            logging.error(e)
            flash(
                'Invalid username or password. Please try again.',
                'danger')
            return render_template('login.html', form=form)

        login_user(user)
        flash('You have successfully logged in.', 'success')
        return redirect(url_for('auth.settings'))

    if form.errors:
        flash(form.errors, 'danger')

    return render_template('login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))
