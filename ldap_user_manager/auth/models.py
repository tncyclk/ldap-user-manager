from ldap3 import Server, Connection, SIMPLE, ALL, BASE, AUTO_BIND_NO_TLS, MODIFY_REPLACE
from flask_wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import InputRequired, EqualTo, Optional
from flask import Flask, current_app as app
from ..ldap_user_manager import db


def get_connection(host, port, user, password):
    server = Server(host=host, port=port)
    conn = Connection(
        server,
        user=user,
        password=password,
        auto_bind=app.config['LDAP_AUTO_BIND'],
        authentication=app.config['LDAP_AUTHENTICATION'])
    conn.open()
    conn.bind()

    return conn


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100))
    sieve = db.Column(db.Text())
    spamassassin = db.Column(db.Text())
    has_mail = db.Column(db.Boolean())

    def __init__(self, username, password, sieve, spamassassin, has_mail):
        self.username = username
        self.sieve = sieve
        self.spamassassin = spamassassin
        self.has_mail = has_mail

    @staticmethod
    def try_login(username, password):
        user = app.config['LDAP_USER_TEMPLATE'].format(username)
        conn = get_connection(app.config['LDAP_HOST'], app.config[
                              'LDAP_PORT'], user, password)
        conn.search(user, '(cn=' + username + ')',
                    attributes=['spamassassin', 'sieve', 'memberOf'])

        return conn.response[0]['attributes']

    def save(self, current_password, sieve, spamassassin, password_hash):
        user = app.config['LDAP_USER_TEMPLATE'].format(self.username)
        conn = get_connection(app.config['LDAP_HOST'], app.config[
                              'LDAP_PORT'], user, current_password)

        conn.modify(user, {
            'sieve': [(MODIFY_REPLACE, [sieve])],
            'spamassassin': [(MODIFY_REPLACE, spamassassin.split('\n'))]
        })

        if password_hash is not None:
            conn.modify(user, {
                'userPassword': [(MODIFY_REPLACE, [password_hash])]
            })

        self.sieve = sieve
        self.spamassassin = spamassassin

        db.session.commit()

        conn.unbind()

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id


class LoginForm(Form):
    username = TextField('Username', [InputRequired()])
    password = PasswordField('Password', [InputRequired()])


class SettingsForm(Form):
    current_password = PasswordField('Current Password', [InputRequired()])
    new_password = PasswordField('New Password', [
        EqualTo('repeat_password', message='Passwords must match')
    ])
    repeat_password = PasswordField('Repeat Password')

    sieve = TextField('Sieve')
    spamassassin = TextField('Spamassassin')
