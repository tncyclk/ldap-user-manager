from setuptools import setup

setup(
    name='ldap_user_manager',
    packages=['ldap_user_manager'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
