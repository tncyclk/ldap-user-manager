# LDAP User Self Management

A small and very simple [flask](http://flask.pocoo.org/) application to allow
ldap users to manage some of their settings themselves.

As I developed this for my own needs you can't configure everything that is
desirable. It should not be too difficult to change that though if someone wants
to.

To understand the ldap configuration take a look at the
[ldap3](http://ldap3.readthedocs.io/) documentation and for the `PASSWORD_SCHEME`
config look at [passlib](https://pythonhosted.org/passlib/).

![screenshot](screenshot.png)
